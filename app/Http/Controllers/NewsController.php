<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Language;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Image;
class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::with('companies','language')->paginate(25);
        return view('news.index',compact('news'));
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $languages = Language::all();
        $companies = Company::all();
        return view('news.add_news',compact('languages','companies'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  request()
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return request()->all();
        request()->validate( [
            'url'              => 'required|url',
            'language'          => 'required',
            'title'          => 'required',
            'summary'          => 'required',
            'file'          => 'required',
            'companies'     =>'required',
        ]);

   
        $image_name = 'img_'.time().'.'.request()->file('file')->getClientOriginalExtension();
        $image = Image::make(request()->file('file'));
        $image->save(public_path("news_images/$image_name"));
        

        $news = new News();
        $news->url = request()->url;
        $news->language = request()->language;
        $news->title = request()->title;
        $news->summary = request()->summary;
        $news->image = $image_name;
        $news->save();
     
        $news->companies()->sync(request()->companies);

        \Session::flash('success_msg','News saved successfully!');
        return redirect()->route('news.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::with('companies','language')->find($id);
        $selected = $news->companies->pluck('id')->toArray();
        $languages = Language::all();
        $companies = Company::all();
        return view('news.edit_news',compact('news','languages','companies','selected'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        request()->validate( [
            'url'              => 'required|url',
            'language'          => 'required',
            'title'          => 'required',
            'summary'          => 'required',
            'companies'     =>'required',
        ]);

        $news = News::with("companies")->find($id);
        
        if(isset(request()->file) AND !empty(request()->file)){
            $image_name = 'img_'.time().'.'.request()->file('file')->getClientOriginalExtension();
            $image = Image::make(request()->file('file'));
            $image->save(public_path("news_images/$image_name"));

            $news->image = $image_name;

        }

        $news->url = request()->url;
        $news->language = request()->language;
        $news->title = request()->title;
        $news->summary = request()->summary;
        $news->save();
        
        $news->companies()->sync(request()->companies);

        \Session::flash('success_msg','News saved successfully!');
        return redirect()->route('news.index');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
