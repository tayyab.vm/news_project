<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;

class CompaniesController extends Controller
{
    public function index(){
        // return 'hit';
        $companies = \App\Models\Company::paginate(25);
        return view('companies.index',compact('companies'));
    }
    public function create(Request $request)
    {

    }
}
