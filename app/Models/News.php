<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;
    public function companies(){
        return $this->belongsToMany(Company::class,'company_new','company_id', 'news_id');
    }
    public function language(){
        return $this->belongsTo(Language::class,'language','id');
    }
}


