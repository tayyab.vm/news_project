<h4>How to setup this project on local development enviroment</h4>

<p>1- Clone this repo</p>
<p>2- cd {project}</p>
<p>3- composer install</p>
<p>4- npm install</p>
<p>5- npm run dev</p>
<p>5- edit .env (according to you system)</p>
<p>6- run "php artisan migrate"</p>
<p>7- run "php artisan db:seed"</p>