<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit News') }}
        </h2>
    </x-slot>

    <div class="py-12">
      <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      {{--  <div class="block mb-8">
          <a type="button" href="{{route('news')}}" class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-green-600 text-base font-medium text-white focus:outline-none focus:ring-2 focus:ring-offset-2 sm:ml-3 sm:w-auto sm:text-sm">
            Return
          </a>
        </div>--}}

          <form enctype="multipart/form-data" method="post" action="{{route('news.update',$news->id)}}" class="w-full max-w-lg">
            @method('put')
              @csrf
              <div class="flex flex-wrap -mx-3 mb-2">
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
                        Select Language
                    </label>
                    <select  class=" appearance-none w-full  border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" name="language" value="{{ old('language') }}">
                        <option value="">Select Language</option>
                         @foreach($languages as $language)
                             <option {{$language->id == $news->language ? 'selected' : ''}} class="capitalize" value="{{$language->id}}">{{$language->name}}</option>
                         @endforeach
                     </select>
                    @if ($errors->has('language'))
                        <span class="text-danger" style="color: red">{{ $errors->first('language') }}</span>
                    @endif
                </div>
            </div>
              <div class="flex flex-wrap -mx-3 mb-2">
                  <div class="w-full px-3">
                      <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
                          Title
                      </label>
                      <input class="appearance-none block w-full  text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-password" type="text" name="title" value="{{ $news->title }}" placeholder="Enter Title Here">
                      @if ($errors->has('title'))
                          <span class="text-danger" style="color: red">{{ $errors->first('title') }}</span>
                      @endif
                  </div>
              </div>
              <div class="flex flex-wrap -mx-3 mb-2">
                  <div class="w-full px-3">
                      <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
                          Url
                      </label>
                      <input class="appearance-none block w-full  text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-password" value="{{$news->url}}" type="url" name="url" placeholder="Enter Title Here">
                      @if ($errors->has('url'))
                          <span class="text-danger" style="color: red">{{ $errors->first('url') }}</span>
                      @endif
                  </div>
              </div>

              <div class="flex  -mx-3 mb-2">
                  <div class="w-full px-3">
                      <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
                          Summary
                      </label>
                      <textarea  name="summary" class="form-textarea mt-1  w-full" rows="3" placeholder="Enter some long form content.">{{$news->summary}}</textarea>
                      @if ($errors->has('summary'))
                          <span class="text-danger" style="color: red">{{ $errors->first('summary') }}</span>
                      @endif
                  </div>
              </div>
              <div class="flex flex-wrap -mx-3 mb-2">
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
                        Select Companies
                    </label>
                  
                     <select multiple class="select2 appearance-none w-full  border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" name="companies[]">
                        <option value="">Select Companies</option>
                        @foreach($companies as $company)
                            <option {{in_array($company->id,$selected) ? 'selected' : ''}} class="capitalize" value="{{$company->id}}">{{$company->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('companies'))
                        <span class="text-danger" style="color: red">{{ $errors->first('companies') }}</span>
                    @endif
                </div>
            </div>
              <div class="flex flex-wrap -mx-3 mb-2">
                  <div class="w-full px-3">
                      <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
                         Image
                      </label>
                      <input class="appearance-none block w-full  text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-password" type="file" name="file">
                      @if ($errors->has('file'))
                      <span class="text-danger" style="color: red">{{ $errors->first('file') }}</span>
                        @endif
                  </div>
              </div>
              <button class="flex-shrink-0 bg-green-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-2 rounded" type="submit">
                  Submit
              </button>

          </form>

      </div>
    </div>
</x-app-layout>
